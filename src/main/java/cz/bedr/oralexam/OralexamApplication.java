package cz.bedr.oralexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OralexamApplication {

    public static void main(String[] args) {
        SpringApplication.run(OralexamApplication.class, args);
    }

}

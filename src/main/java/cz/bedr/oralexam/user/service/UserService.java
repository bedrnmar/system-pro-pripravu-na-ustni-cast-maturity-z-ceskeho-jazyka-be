package cz.bedr.oralexam.user.service;

import cz.bedr.oralexam.security.dto.SignupRequest;
import cz.bedr.oralexam.user.dm.ERole;
import cz.bedr.oralexam.user.dm.Role;
import cz.bedr.oralexam.user.dm.User;
import cz.bedr.oralexam.user.repository.RoleRepository;
import cz.bedr.oralexam.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    public User saveUser(SignupRequest signupRequest) {

        if (userRepository.existsByUsername(signupRequest.getUsername())) {
            throw new IllegalArgumentException("User already exists");
        }

        User user = new User(signupRequest.getUsername(), encoder.encode(signupRequest.getPassword()));

        Set<Role> roles = new HashSet<>();

        Role userRole = roleRepository.findByName(ERole.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        roles.add(userRole);

        user.setRoles(roles);
        return userRepository.save(user);
    }
}

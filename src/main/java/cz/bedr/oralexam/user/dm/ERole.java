package cz.bedr.oralexam.user.dm;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}

package cz.bedr.oralexam.user.repository;
import java.util.Optional;

import cz.bedr.oralexam.user.dm.ERole;
import cz.bedr.oralexam.user.dm.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}

package cz.bedr.oralexam.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class UserResponse {
    private long id;
    private String username;
    private String jwt;
    private List<String> roles;
}

package cz.bedr.oralexam.security.controller;

import cz.bedr.oralexam.security.dto.AuthRequest;
import cz.bedr.oralexam.security.dto.SignupRequest;
import cz.bedr.oralexam.security.dto.UserResponse;
import cz.bedr.oralexam.security.service.UserDetailsImpl;
import cz.bedr.oralexam.security.util.JwtUtils;
import cz.bedr.oralexam.user.dm.User;
import cz.bedr.oralexam.user.repository.RoleRepository;
import cz.bedr.oralexam.user.repository.UserRepository;
import cz.bedr.oralexam.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/public")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserService userService;

    @PostMapping("/login")
    public UserResponse authenticateUser(@RequestBody AuthRequest authRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return new UserResponse(userDetails.getId(), userDetails.getUsername(), jwt, roles);
    }

    @PostMapping("/register")
    public User registerUser(@RequestBody SignupRequest signUpRequest) {
        return userService.saveUser(signUpRequest);
    }
}

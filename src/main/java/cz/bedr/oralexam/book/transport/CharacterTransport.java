package cz.bedr.oralexam.book.transport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CharacterTransport {
    private String name;
    private String description;
}

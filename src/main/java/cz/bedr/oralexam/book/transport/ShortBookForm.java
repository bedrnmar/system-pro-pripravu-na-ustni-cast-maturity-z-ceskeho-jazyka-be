package cz.bedr.oralexam.book.transport;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ShortBookForm {
    long id;
    String name;
    String form;
    String category;
    String author;
}

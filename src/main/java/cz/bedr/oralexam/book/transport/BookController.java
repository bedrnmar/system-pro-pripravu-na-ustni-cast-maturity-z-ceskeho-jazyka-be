package cz.bedr.oralexam.book.transport;

import cz.bedr.oralexam.book.dm.Book;
import cz.bedr.oralexam.book.dm.ContextInfo;
import cz.bedr.oralexam.book.service.BookService;
import cz.bedr.oralexam.controllers.PublicController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class BookController extends PublicController {
    @Autowired
    BookService bookService;


    @PostMapping(path = PUBLIC_PREFIX + "/create-book")
    public Book createBook(@RequestBody CompleteBookForm completeBookForm){
        return bookService.saveBook(completeBookForm);
    }

    @PutMapping(path = PUBLIC_PREFIX + "/edit-book")
    public Book updateBook(@RequestParam long id,@RequestBody CompleteBookForm completeBookForm) {
        return bookService.updateBook(id, completeBookForm);
    }

    @GetMapping(path = PUBLIC_PREFIX + "/book-list")
    public List<ShortBookForm> getAllBooks() {
        return bookService.getBooks();
    }

    @PostMapping(path = PUBLIC_PREFIX + "/book-history")
    public List<ContextForm> getHistoryContexts(@RequestBody ContextInfo contextInfo){
        return bookService.getContexts("history", contextInfo);
    }

    @PostMapping(path = PUBLIC_PREFIX + "/book-literary")
    public List<ContextForm> getLiteraryContexts(@RequestBody ContextInfo contextInfo){
        return bookService.getContexts("literary", contextInfo);
    }

    @PostMapping(path = PUBLIC_PREFIX + "/contemporary-authors")
    public List<String> getContemporaryAuthors(@RequestBody ContextInfo contextInfo){
        return bookService.getContemporaryAuthors(contextInfo);
    }

    @GetMapping(path = PUBLIC_PREFIX + "/book-detail")
    public CompleteBookForm getBook(@RequestParam long id) {
        return bookService.getBookById(id);
    }

    @DeleteMapping(path = "/delete-book")
    public void deleteBook(@RequestParam long id) {
        bookService.deleteBookById(id);
    }
}

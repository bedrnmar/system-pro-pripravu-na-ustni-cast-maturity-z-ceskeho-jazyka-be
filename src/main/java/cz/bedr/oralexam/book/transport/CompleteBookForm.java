package cz.bedr.oralexam.book.transport;

import cz.bedr.oralexam.book.dm.Book;
import cz.bedr.oralexam.book.dm.Character;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompleteBookForm {
    private String bookName;
    private String litForm;
    private List<String> litGenres;
    private int firstEdition;
    private String isCzechOrWorld;
    private String authorName;
    private String authorInfo;
    private List<String> authorBooksDb = new ArrayList<>();
    private List<String> authorBooksOther = new ArrayList<>();
    private String historyContext;
    private String literaryContext;
    private List<String> contemporaryAuthors;
    private String theme;
    private String motives;
    private String space;
    private String time;
    private String plot;
    private List<CharacterTransport> characters = new ArrayList<>();
    private String composition;
    private String extraInfo;

    public CompleteBookForm(Book book){
        this.bookName = book.getName();
        this.litForm = book.getLiteraryForm().name();
        this.litGenres = book.getGenres();
        this.firstEdition = book.getFirstEdition();
        this.isCzechOrWorld = book.getOrigin().name();
        if(book.getAuthor() != null) {
            this.authorName = book.getAuthor().getName();
            this.authorInfo = book.getAuthor().getInfo();
            for (Book dbBook : book.getAuthor().getDatabaseBooks()) {
                this.authorBooksDb.add(dbBook.getName());
            }
            this.authorBooksOther = book.getAuthor().getOtherBooks();
        }
        this.historyContext = book.getHistoryContext();
        this.literaryContext = book.getLiteraryContext();
        this.contemporaryAuthors = book.getContemporaryAuthors();
        if(book.getBookContent() != null) {
            this.theme = book.getBookContent().getTheme();
            this.motives = book.getBookContent().getMotives();
            this.space = book.getBookContent().getSpace();
            this.time = book.getBookContent().getTime();
            this.plot = book.getBookContent().getPlot();
            for(Character character :  book.getBookContent().getCharacters()){
                this.characters.add(new CharacterTransport(character.getName(), character.getDescription()));
            }
            this.composition = book.getBookContent().getComposition();
            this.extraInfo = book.getBookContent().getExtraInfo();
        }
    }
}

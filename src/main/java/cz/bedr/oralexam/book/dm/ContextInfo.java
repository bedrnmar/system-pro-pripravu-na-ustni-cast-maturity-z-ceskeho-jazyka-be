package cz.bedr.oralexam.book.dm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContextInfo {
    String authorName;
    int firstEdition;
    boolean isCzech;
}

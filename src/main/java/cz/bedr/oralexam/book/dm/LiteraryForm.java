package cz.bedr.oralexam.book.dm;

public enum LiteraryForm {
    POETRY,
    PROSE,
    DRAMA
}

package cz.bedr.oralexam.book.dm;

public enum Category {
    UNTIL18,
    ALL19,
    WORLD20,
    CZECH20
}

package cz.bedr.oralexam.book.dm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.bedr.oralexam.author.dm.Author;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Data
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int firstEdition;

    @Enumerated(EnumType.STRING)
    @Column(length = 10, nullable = false)
    private Origin origin;

    @ElementCollection
    private List<String> genres = new ArrayList<>();

    @ElementCollection
    private List<String> contemporaryAuthors = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private LiteraryForm literaryForm;

    @Column(columnDefinition = "TEXT")
    private String literaryContext;

    @Column(columnDefinition = "TEXT")
    private String historyContext;

    @ManyToOne
    private Author author;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "content_id")
    private BookContent bookContent;

    public Book(String name, LiteraryForm literaryForm, Origin origin, int firstEdition, List<String> genres, String literaryContext, String historyContext, List<String> contemporaryAuthors, BookContent bookContent){
        this.name = name;
        this.literaryForm = literaryForm;
        this.origin = origin;
        this.firstEdition = firstEdition;
        this.genres = genres;
        this.literaryContext = literaryContext;
        this.historyContext = historyContext;
        this.contemporaryAuthors = contemporaryAuthors;
        this.bookContent = bookContent;
    }
}

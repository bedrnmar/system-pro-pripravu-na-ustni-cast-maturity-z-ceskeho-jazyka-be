package cz.bedr.oralexam.book.dm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.bedr.oralexam.book.transport.CharacterTransport;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(exclude="book")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class BookContent {
    @Id
    @GeneratedValue
    private long id;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "bookContent")
    private Book book;

    @Column(columnDefinition = "TEXT")
    private String theme;

    @Column(columnDefinition = "TEXT")
    private String motives;

    @Column(columnDefinition = "TEXT")
    private String space;

    @Column(columnDefinition = "TEXT")
    private String time;

    @Column(columnDefinition = "TEXT")
    private String plot;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "char_fid", referencedColumnName = "id")
    private List<Character> characters = new ArrayList<>();

    @Column(columnDefinition = "TEXT")
    private String composition;

    @Column(columnDefinition = "TEXT")
    private String extraInfo;

    public BookContent(String theme, String motives, String space, String time, String plot, List<CharacterTransport> characters, String composition, String extraInfo) {
        this.theme = theme;
        this.motives = motives;
        this.space = space;
        this.time = time;
        this.plot = plot;
        if(characters != null) {
            for (CharacterTransport characterTransport : characters) {
                this.characters.add(new Character(characterTransport.getName(), characterTransport.getDescription()));
            }
        }
        this.composition = composition;
        this.extraInfo = extraInfo;
    }
}

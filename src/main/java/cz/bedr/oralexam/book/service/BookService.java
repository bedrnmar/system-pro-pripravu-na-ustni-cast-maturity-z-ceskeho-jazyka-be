package cz.bedr.oralexam.book.service;

import cz.bedr.oralexam.author.dm.Author;
import cz.bedr.oralexam.author.repository.AuthorRepository;
import cz.bedr.oralexam.book.dm.Character;
import cz.bedr.oralexam.book.dm.*;
import cz.bedr.oralexam.book.repository.BookRepository;
import cz.bedr.oralexam.book.transport.CharacterTransport;
import cz.bedr.oralexam.book.transport.CompleteBookForm;
import cz.bedr.oralexam.book.transport.ContextForm;
import cz.bedr.oralexam.book.transport.ShortBookForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BookService {
    @Autowired
    BookRepository bookRepository;
    @Autowired
    AuthorRepository authorRepository;

    public Book saveBook(CompleteBookForm bookForm) {
        BookContent bookContent = new BookContent(bookForm.getTheme(),
                bookForm.getMotives(), bookForm.getSpace(), bookForm.getTime(),
                bookForm.getPlot(), bookForm.getCharacters(), bookForm.getComposition(), bookForm.getExtraInfo());

        Author author = null;
        if(bookForm.getAuthorName() != null) {
             author = authorRepository.findAuthorByName(bookForm.getAuthorName()).orElse(null);
            if (author == null) {
                author = new Author(bookForm.getAuthorName(), bookForm.getAuthorInfo(), bookForm.getAuthorBooksOther());
            } else {
                author.setInfo(bookForm.getAuthorInfo());
                author.setOtherBooks(bookForm.getAuthorBooksOther());
            }
        }

        LiteraryForm literaryForm = getForm(bookForm.getLitForm());
        Origin origin = bookForm.getIsCzechOrWorld().equals("czech") ? Origin.CZECH : Origin.WORLD;

        Book newBook = new Book(bookForm.getBookName(), literaryForm, origin, bookForm.getFirstEdition(), bookForm.getLitGenres(),
                bookForm.getLiteraryContext(), bookForm.getHistoryContext(), bookForm.getContemporaryAuthors(), bookContent);

        if(author!=null) {
            authorRepository.save(author);
            newBook.setAuthor(author);
        }
        return bookRepository.save(newBook);
    }

    public Book updateBook(long id, CompleteBookForm completeBookForm) {
        Book existingBook = bookRepository.findById(id).orElse(null);
        if(existingBook == null){
            throw new IllegalArgumentException("Book with id: "+ id + "does not exist");
        }
        existingBook.setName(completeBookForm.getBookName());
        existingBook.setLiteraryForm(getForm(completeBookForm.getLitForm()));
        existingBook.setGenres(completeBookForm.getLitGenres());
        existingBook.setFirstEdition(completeBookForm.getFirstEdition());
        existingBook.setOrigin(completeBookForm.getIsCzechOrWorld().equals("czech") ? Origin.CZECH : Origin.WORLD);

        Author author = authorRepository.findAuthorByName(completeBookForm.getAuthorName()).orElse(null);
        if(author != null){
            author.setInfo(completeBookForm.getAuthorInfo());
            author.setOtherBooks(completeBookForm.getAuthorBooksOther());
        } else{
            author = authorRepository.save(new Author(completeBookForm.getAuthorName(), completeBookForm.getAuthorInfo(), completeBookForm.getAuthorBooksOther()));
        }
        existingBook.setAuthor(author);
        existingBook.setHistoryContext(completeBookForm.getHistoryContext());
        existingBook.setLiteraryContext(completeBookForm.getLiteraryContext());
        existingBook.setContemporaryAuthors(completeBookForm.getContemporaryAuthors());

        BookContent content = existingBook.getBookContent();
        if(content == null){
            content = new BookContent();
        }
        content.setTheme(completeBookForm.getTheme());
        content.setMotives(completeBookForm.getMotives());
        content.setSpace(completeBookForm.getSpace());
        content.setTime(completeBookForm.getTime());
        content.setPlot(completeBookForm.getPlot());
        content.setCharacters(new ArrayList<>());
        for(CharacterTransport characterTransport: completeBookForm.getCharacters()) {
            content.getCharacters().add(new Character(characterTransport.getName(), characterTransport.getDescription()));
        }
        content.setComposition(completeBookForm.getComposition());
        content.setExtraInfo(completeBookForm.getExtraInfo());
        existingBook.setBookContent(content);

        return bookRepository.save(existingBook);
    }

    public List<ShortBookForm> getBooks() {
        List<Book> books = bookRepository.findAll();
        List<ShortBookForm> booksList = new ArrayList<>();
        for(Book book: books){
            if(book.getAuthor()!= null) {
                booksList.add(new ShortBookForm(book.getId(), book.getName(), book.getLiteraryForm().name(), getCategory(book.getFirstEdition(), book.getOrigin()).name(), book.getAuthor().getName()));
            } else {
                booksList.add(new ShortBookForm(book.getId(), book.getName(), book.getLiteraryForm().name(), getCategory(book.getFirstEdition(), book.getOrigin()).name(), "Neznámý autor"));
            }
        }
        return booksList;
    }

    public List<ContextForm> getContexts(String type, ContextInfo contextInfo){
        Set<Book> matchedBooks = new LinkedHashSet<>();
        Origin origin = contextInfo.isCzech() ? Origin.CZECH : Origin.WORLD;
        Origin counterOrigin = contextInfo.isCzech() ? Origin.WORLD : Origin.CZECH;

        if(contextInfo.getAuthorName() != null) {
            Author author = authorRepository.findAuthorByName(contextInfo.getAuthorName()).orElse(null);
            if (author != null) {
                matchedBooks.addAll(bookRepository.getAllByAuthor(author));
            }
        }

        if(contextInfo.getFirstEdition() < 1800) {
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(contextInfo.getFirstEdition() - 100, contextInfo.getFirstEdition() + 100, origin));
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(contextInfo.getFirstEdition() - 100, contextInfo.getFirstEdition() + 100, counterOrigin));
        } else if(contextInfo.getFirstEdition() < 1900) {
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(contextInfo.getFirstEdition() - 50, contextInfo.getFirstEdition() + 50, origin));
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(contextInfo.getFirstEdition() - 50, contextInfo.getFirstEdition() + 50, counterOrigin));
        } else {
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(contextInfo.getFirstEdition() - 30, contextInfo.getFirstEdition() + 30, origin));
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(contextInfo.getFirstEdition() - 30, contextInfo.getFirstEdition() + 30, counterOrigin));
        }

        List<ContextForm> contexts = new ArrayList<>();
        for (Book book: matchedBooks){
            if(type.equals("history")){
                contexts.add(new ContextForm(book.getName(), book.getHistoryContext()));
            } else if (type.equals("literary")){
                contexts.add(new ContextForm(book.getName(), book.getLiteraryContext()));
            }
        }
        return contexts;
    }

    public List<String> getContemporaryAuthors(ContextInfo contextInfo){
        Set<String> authorNames = new LinkedHashSet<>();
        int maxSize = 5;

        if(contextInfo.getAuthorName() != null){
            authorNames.add(contextInfo.getAuthorName());
            maxSize++;
        }

        int nbOfRounds = 0;
        int middleOfLower = contextInfo.getFirstEdition() - 5;
        int middleOfUpper = contextInfo.getFirstEdition() + 5;
        Origin origin = contextInfo.isCzech() ? Origin.CZECH : Origin.WORLD;
        Origin counterOrigin = contextInfo.isCzech() ? Origin.WORLD : Origin.CZECH;
        List<Book> matchedBooks = new ArrayList<>();

        while (nbOfRounds < 7){
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(middleOfLower - 5,
                    middleOfLower + 5, origin));
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(middleOfUpper - 5,
                    middleOfUpper + 5, origin));
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(middleOfLower - 5,
                    middleOfLower + 5, counterOrigin));
            matchedBooks.addAll(bookRepository.getAllByFirstEditionBetweenAndOrigin(middleOfUpper - 5,
                    middleOfUpper + 5, counterOrigin));

            for (Book book : matchedBooks){
                authorNames.add(book.getAuthor().getName());
                if (authorNames.size() > maxSize){
                    break;
                }
            }

            matchedBooks.clear();
            middleOfUpper += 10;
            middleOfLower -= 10;
            ++nbOfRounds;
        }

        authorNames.remove(contextInfo.getAuthorName());

        return new ArrayList<>(authorNames);
    }

    public CompleteBookForm getBookById(Long id) {
        Book foundBook = bookRepository.findById(id).orElse(null);
        if(foundBook != null){
            return new CompleteBookForm(foundBook);
        } else {
            throw new IllegalArgumentException("Book wasn't found in database");
        }
    }

    public void deleteBookById(Long id) {
        bookRepository.deleteById(id);
    }

    private Category getCategory(int firstEdition, Origin origin){
        if(firstEdition <= 1800){
            return Category.UNTIL18;
        } else if (firstEdition <= 1900){
            return Category.ALL19;
        } else if (origin.equals(Origin.CZECH)) {
            return Category.CZECH20;
        } else {
            return Category.WORLD20;
        }
    }

    private LiteraryForm getForm(String litForm){
        if(litForm.equals("Próza")){
            return LiteraryForm.PROSE;
        } else if(litForm.equals("Poezie")){
            return LiteraryForm.POETRY;
        } else {
            return LiteraryForm.DRAMA;
        }
    }
}

package cz.bedr.oralexam.book.repository;

import cz.bedr.oralexam.book.dm.BookContent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookContentRepository extends JpaRepository<BookContent, Long> {
}

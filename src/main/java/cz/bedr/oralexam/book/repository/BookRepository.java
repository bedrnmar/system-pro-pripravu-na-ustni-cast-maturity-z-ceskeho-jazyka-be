package cz.bedr.oralexam.book.repository;

import cz.bedr.oralexam.author.dm.Author;
import cz.bedr.oralexam.book.dm.Book;
import cz.bedr.oralexam.book.dm.Origin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> getAllByAuthor(Author author);
    List<Book> getAllByFirstEditionBetweenAndOrigin(int lowerBound, int upperBound, Origin origin);
}

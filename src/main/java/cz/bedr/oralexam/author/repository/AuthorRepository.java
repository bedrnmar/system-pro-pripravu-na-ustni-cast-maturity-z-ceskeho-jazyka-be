package cz.bedr.oralexam.author.repository;

import cz.bedr.oralexam.author.dm.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author, Long> {
    Optional<Author> findAuthorByName(String name);
    Author findAuthorById(Long id);
}

package cz.bedr.oralexam.author.transport;

import cz.bedr.oralexam.author.dm.Author;
import cz.bedr.oralexam.author.service.AuthorService;
import cz.bedr.oralexam.controllers.PublicController;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class AuthorController extends PublicController {
    @Autowired
    AuthorService authorService;

    @PostMapping(path = PUBLIC_PREFIX + "/create-author")
    public Author addAuthor(@RequestBody AuthorForm authorForm) {
        return authorService.addAuthor(authorForm);
    }

    @PutMapping(path = PUBLIC_PREFIX + "/edit-author")
    public Author updateAuthor(@RequestBody AuthorFullForm authorFullForm) {
        return authorService.updateAuthorById(authorFullForm);
    }

    @GetMapping(path = PUBLIC_PREFIX + "/author-list")
    public List<AuthorFullForm> getAllAuthors() {
        return authorService.getAuthors();
    }

    @GetMapping(path = PUBLIC_PREFIX + "/author-detail")
    public AuthorFullForm getAuthor(@RequestParam long id) {
        return authorService.getAuthorById(id);
    }

    @DeleteMapping(path =  "/delete-author")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteAuthor(@RequestParam long id) {
        authorService.deleteAuthorById(id);
    }
}

package cz.bedr.oralexam.author.transport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorForm {
    long id;
    String name;
    String info;
    List<String> books;
}

package cz.bedr.oralexam.author.dm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.bedr.oralexam.book.dm.Book;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Data
public class Author {
    @Id
    @GeneratedValue
    private Long id;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String info;

    @Column(unique = true, nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "author")
    private List<Book> databaseBooks = new ArrayList<>();

    @ElementCollection
    private List<String> otherBooks = new ArrayList<>();

    public Author(String name, String info, List<String> books) {
        this.name = name;
        this.info = info;
        this.otherBooks.addAll(books);
    }

    public Author() { }
}

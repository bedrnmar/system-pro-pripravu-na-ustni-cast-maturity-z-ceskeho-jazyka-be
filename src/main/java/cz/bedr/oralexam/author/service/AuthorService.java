package cz.bedr.oralexam.author.service;

import cz.bedr.oralexam.author.dm.Author;
import cz.bedr.oralexam.author.repository.AuthorRepository;
import cz.bedr.oralexam.author.transport.AuthorForm;
import cz.bedr.oralexam.author.transport.AuthorFullForm;
import cz.bedr.oralexam.book.dm.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorService {
    @Autowired
    AuthorRepository authorRepository;

    public Author addAuthor(AuthorForm authorForm) {
        Author newAuthor = new Author(authorForm.getName(), authorForm.getInfo(), authorForm.getBooks());
        return authorRepository.save(newAuthor);
    }

    public Author updateAuthorById(AuthorFullForm authorFullForm) {
        Author existingAuthor = authorRepository.findById(authorFullForm.getId()).orElse(null);
        if (existingAuthor == null)
            throw new IllegalArgumentException("Author does not exist");
        if (authorFullForm.getName() == null)
            throw new IllegalArgumentException("Author can't have empty name");
        if (authorFullForm.getInfo() == null)
            throw new IllegalArgumentException("Author can't have empty info");
        existingAuthor.setName(authorFullForm.getName());
        existingAuthor.setInfo(authorFullForm.getInfo());

        existingAuthor.setOtherBooks(authorFullForm.getOtherBooks());
        return authorRepository.save(existingAuthor);
    }

    public List<AuthorFullForm> getAuthors() {
         List<Author> authors = authorRepository.findAll();
         List<AuthorFullForm> authorViews = new ArrayList<>();
         for(Author author: authors){
             List<String> dbBooksNames = new ArrayList<>();
             for(Book authorBook: author.getDatabaseBooks()){
                 dbBooksNames.add(authorBook.getName());
             }
             AuthorFullForm authorView = new AuthorFullForm(author.getId(), author.getName(), author.getInfo(), dbBooksNames, author.getOtherBooks());
             authorViews.add(authorView);
         }
         return authorViews;
    }

    public AuthorFullForm getAuthorById(Long id){
        Author author =  authorRepository.findById(id).orElse(null);
        if(author != null) {
            List<String> authorDbBooks = new ArrayList<>();
            for (Book book : author.getDatabaseBooks()){
                authorDbBooks.add(book.getName());
            }
            return new AuthorFullForm(author.getId(), author.getName(), author.getInfo(), authorDbBooks, author.getOtherBooks());
        } else {
            throw new IllegalArgumentException("Author with this id does not exist");
        }
    }

    public void deleteAuthorById(Long id) {
        authorRepository.deleteById(id);
    }

    List<String> getAuthorBooks(Author author){
        List<String> allBookNames = new ArrayList<>(author.getOtherBooks());
        for (Book book: author.getDatabaseBooks()){
            allBookNames.add(book.getName());
        }
        return allBookNames;
    }

}

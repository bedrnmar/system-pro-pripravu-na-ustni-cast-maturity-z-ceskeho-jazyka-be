package cz.bedr.oralexam.authTest;

import cz.bedr.oralexam.security.dto.SignupRequest;
import cz.bedr.oralexam.security.service.UserDetailsImpl;
import cz.bedr.oralexam.security.service.UserDetailsServiceImpl;
import cz.bedr.oralexam.security.util.JwtUtils;
import cz.bedr.oralexam.user.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class AuthServiceTest {
    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    UserService userService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;


    @Transactional
    @Test
    void loginUser(){
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("User1");
        signupRequest.setPassword("Password");
        userService.saveUser(signupRequest);

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(signupRequest.getUsername(), signupRequest.getPassword()));

        assertThat(authentication).isNotNull();

        String jwt = jwtUtils.generateJwtToken(authentication);

        assertThat(jwt).isNotNull();

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        assertThat(userDetails).isNotNull();
    }

    @Transactional
    @Test
    void loginUserWithWrongCredentialsShouldFail(){
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("User1");
        signupRequest.setPassword("Password");
        userService.saveUser(signupRequest);

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(signupRequest.getUsername(), "Password1"));
            Assertions.fail();
        } catch (AuthenticationException ignored){}
    }

    @Transactional
    @Test
    void loginUserWithWrongCredentialsShouldFail2(){
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("User1");
        signupRequest.setPassword("Password");
        userService.saveUser(signupRequest);

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken("User2", signupRequest.getPassword()));
            Assertions.fail();
        } catch (AuthenticationException ignored){}
    }
}

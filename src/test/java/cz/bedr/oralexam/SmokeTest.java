package cz.bedr.oralexam;

import cz.bedr.oralexam.author.repository.AuthorRepository;
import cz.bedr.oralexam.author.service.AuthorService;
import cz.bedr.oralexam.author.transport.AuthorController;
import cz.bedr.oralexam.book.repository.BookRepository;
import cz.bedr.oralexam.book.service.BookService;
import cz.bedr.oralexam.book.transport.BookController;
import cz.bedr.oralexam.security.controller.AuthController;
import cz.bedr.oralexam.security.service.UserDetailsServiceImpl;
import cz.bedr.oralexam.user.repository.RoleRepository;
import cz.bedr.oralexam.user.repository.UserRepository;
import cz.bedr.oralexam.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SmokeTest {
    @Autowired
    AuthorController authorController;
    @Autowired
    BookController bookController;
    @Autowired
    AuthController authController;

    @Autowired
    UserService userService;
    @Autowired
    UserDetailsServiceImpl userDetailsService;
    @Autowired
    AuthorService authorService;
    @Autowired
    BookService bookService;

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    BookRepository bookRepository;
    @Autowired
    AuthorRepository authorRepository;

    @Test
    void controllersLoad(){
        assertThat(authController).isNotNull();
        assertThat(bookController).isNotNull();
        assertThat(authController).isNotNull();
    }

    @Test
    void servicesLoad(){
        assertThat(userService).isNotNull();
        assertThat(userDetailsService).isNotNull();
        assertThat(authorService).isNotNull();
        assertThat(bookService).isNotNull();
    }

    @Test
    void repositoriesLoad(){
        assertThat(userRepository).isNotNull();
        assertThat(roleRepository).isNotNull();
        assertThat(bookRepository).isNotNull();
        assertThat(authorRepository).isNotNull();
    }

}

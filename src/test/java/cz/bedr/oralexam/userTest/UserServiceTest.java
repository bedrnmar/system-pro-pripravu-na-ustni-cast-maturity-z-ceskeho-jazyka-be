package cz.bedr.oralexam.userTest;

import cz.bedr.oralexam.security.dto.SignupRequest;
import cz.bedr.oralexam.user.dm.User;
import cz.bedr.oralexam.user.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class UserServiceTest {
    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Transactional
    @Test
    void createNewUser(){
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("User1");
        signupRequest.setPassword("Password");

        User newUser = userService.saveUser(signupRequest);
        assertThat(newUser).isNotNull();
        assertThat(newUser.getUsername()).isEqualTo(signupRequest.getUsername());
        assertThat(newUser.getPassword()).isNotEqualTo(signupRequest.getPassword());
    }

    @Transactional
    @Test
    void createNewUserWithoutUsernameShouldFail(){
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setPassword("Password");

        try {
            userService.saveUser(signupRequest);
            Assertions.fail();
        } catch (DataIntegrityViolationException ignored){}
    }

    @Transactional
    @Test
    void createNewUserWithoutPasswordShouldFail(){
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("User1");

        try {
            userService.saveUser(signupRequest);
            Assertions.fail();
        } catch (DataIntegrityViolationException | IllegalArgumentException ignored){}
    }

}

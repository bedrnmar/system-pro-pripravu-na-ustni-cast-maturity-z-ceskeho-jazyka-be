package cz.bedr.oralexam.authorTest;

import cz.bedr.oralexam.author.dm.Author;
import cz.bedr.oralexam.author.repository.AuthorRepository;
import cz.bedr.oralexam.author.service.AuthorService;
import cz.bedr.oralexam.author.transport.AuthorForm;
import cz.bedr.oralexam.author.transport.AuthorFullForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class AuthorServiceTest {
    @Autowired
    AuthorService authorService;

    @Autowired
    AuthorRepository authorRepository;

    @Test
    @Transactional
    void createNewAuthor(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setName("Karel Pech");
        newAuthor.setInfo("Meet non curabitur gravida arcu ac. Massa sed elementum tempus egestas. Consequat mauris nunc congue nisi vitae suscipit tellus.");
        newAuthor.setBooks(new ArrayList<>(Arrays.asList("Beowulf", "Artush Fall")));
        Author author = authorService.addAuthor(newAuthor);
        assertThat(author).isNotNull();
        assertThat(author.getId()).isNotNull();
    }

    @Test
    @Transactional
    void createAuthorWithoutNameShouldFail(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setInfo("Meet non curabitur gravida arcu ac. Massa sed elementum tempus egestas. Consequat mauris nunc congue nisi vitae suscipit tellus.");
        newAuthor.setBooks(new ArrayList<>(Arrays.asList("Beowulf", "Artush Fall")));
        try {
            authorService.addAuthor(newAuthor);
            Assertions.fail();
        } catch (DataIntegrityViolationException ignored){}
    }

    @Test
    @Transactional
    void createAuthorWithoutInfoShouldFail(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setName("Karel Pech");
        newAuthor.setBooks(new ArrayList<>(Arrays.asList("Beowulf", "Artush Fall")));
        try {
            authorService.addAuthor(newAuthor);
            Assertions.fail();
        } catch (DataIntegrityViolationException ignored){}
    }

    @Test
    @Transactional
    void createAuthorWithoutBooksShouldFail(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setName("Karel Pech");
        newAuthor.setInfo("Meet non curabitur gravida arcu ac. Massa sed elementum tempus egestas. Consequat mauris nunc congue nisi vitae suscipit tellus.");
        try {
            authorService.addAuthor(newAuthor);
            Assertions.fail();
        } catch (NullPointerException | DataIntegrityViolationException ignored){}
    }

    @Test
    @Transactional
    void getAuthor(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setName("Karel Pech");
        newAuthor.setInfo("Meet non curabitur gravida arcu ac. Massa sed elementum tempus egestas. Consequat mauris nunc congue nisi vitae suscipit tellus.");
        newAuthor.setBooks(new ArrayList<>(Arrays.asList("Beowulf", "Artush Fall")));
        Author author = authorService.addAuthor(newAuthor);
        long id1 = author.getId();

        AuthorFullForm authorsFound = authorService.getAuthorById(id1);
        assertThat(authorsFound).isNotNull();
    }

    @Test
    @Transactional
    void getAuthorByInvalidIdShouldFail(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setName("Karel Pech");
        newAuthor.setInfo("Meet non curabitur gravida arcu ac. Massa sed elementum tempus egestas. Consequat mauris nunc congue nisi vitae suscipit tellus.");
        newAuthor.setBooks(new ArrayList<>(Arrays.asList("Beowulf", "Artush Fall")));
        authorService.addAuthor(newAuthor);

        try {
            authorService.getAuthorById(99999L);
            Assertions.fail();
        } catch (IllegalArgumentException ignored){}
    }

    @Test
    @Transactional
    void updateAuthor(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setName("Karel Pech");
        newAuthor.setInfo("Meet non curabitur gravida arcu ac. Massa sed elementum tempus egestas. Consequat mauris nunc congue nisi vitae suscipit tellus.");
        newAuthor.setBooks(new ArrayList<>(Arrays.asList("Beowulf", "Artush Fall")));
        Author author = authorService.addAuthor(newAuthor);
        long id1 = author.getId();

        AuthorFullForm updateAuthorRequest = new AuthorFullForm();
        updateAuthorRequest.setId(id1);
        updateAuthorRequest.setName("Karel Paces");
        updateAuthorRequest.setInfo("Pacesuv paces");
        updateAuthorRequest.setDbBooks(new ArrayList<>());
        updateAuthorRequest.setOtherBooks(new ArrayList<>(Collections.singletonList("U kadere")));

        authorService.updateAuthorById(updateAuthorRequest);
        AuthorFullForm updatedAuthor = authorService.getAuthorById(id1);
        assertThat(updatedAuthor).isEqualTo(updateAuthorRequest);
    }

    @Test
    @Transactional
    void updateAuthorMissingMandatoryInfoShouldFail(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setName("Karel Pech");
        newAuthor.setInfo("Meet non curabitur gravida arcu ac. Massa sed elementum tempus egestas. Consequat mauris nunc congue nisi vitae suscipit tellus.");
        newAuthor.setBooks(new ArrayList<>(Arrays.asList("Beowulf", "Artush Fall")));
        Author author = authorService.addAuthor(newAuthor);

        AuthorFullForm updateAuthorRequest = new AuthorFullForm();
        updateAuthorRequest.setId(author.getId());
        updateAuthorRequest.setInfo("Pacesuv paces");
        updateAuthorRequest.setDbBooks(new ArrayList<>());
        updateAuthorRequest.setOtherBooks(new ArrayList<>(Collections.singletonList("U kadere")));

        try {
            authorService.updateAuthorById(updateAuthorRequest);
            Assertions.fail();
        } catch (IllegalArgumentException ignored){}

    }

    @Test
    @Transactional
    void deleteAuthor(){
        AuthorForm newAuthor = new AuthorForm();

        newAuthor.setName("Karel Pech");
        newAuthor.setInfo("Meet non curabitur gravida arcu ac. Massa sed elementum tempus egestas. Consequat mauris nunc congue nisi vitae suscipit tellus.");
        newAuthor.setBooks(new ArrayList<>(Arrays.asList("Beowulf", "Artush Fall")));
        Author author = authorService.addAuthor(newAuthor);
        long id1 = author.getId();

        authorService.deleteAuthorById(id1);
    }

}

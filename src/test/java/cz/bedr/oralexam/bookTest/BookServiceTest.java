package cz.bedr.oralexam.bookTest;

import cz.bedr.oralexam.book.dm.Book;
import cz.bedr.oralexam.book.dm.ContextInfo;
import cz.bedr.oralexam.book.repository.BookRepository;
import cz.bedr.oralexam.book.service.BookService;
import cz.bedr.oralexam.book.transport.CharacterTransport;
import cz.bedr.oralexam.book.transport.CompleteBookForm;
import cz.bedr.oralexam.book.transport.ContextForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class BookServiceTest {
    @Autowired
    BookService bookService;

    @Autowired
    BookRepository bookRepository;

    @Test
    @Transactional
    void createNewBook(){
        CompleteBookForm newBook = new CompleteBookForm();

        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setLitGenres(new ArrayList<>(Arrays.asList("Fantasy", "Novela")));
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);
        newBook.setAuthorName("Karel Kastan");
        newBook.setAuthorInfo("Spadl z kastanu");
        newBook.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook.setHistoryContext("Velky spatny");
        newBook.setLiteraryContext("Velky dobry");
        newBook.setContemporaryAuthors(new ArrayList<>(Arrays.asList("Pepa", "Vasek")));
        newBook.setTheme("O osudu stromu");
        newBook.setMotives("Padani listi");
        newBook.setTime("Davno");
        newBook.setSpace("V lese");
        newBook.setPlot("O umirani stromu");
        CharacterTransport stromak = new CharacterTransport();
        stromak.setName("Stromak");
        stromak.setDescription("Stromopan");
        CharacterTransport smrk = new CharacterTransport();
        stromak.setName("Smrcek");
        stromak.setDescription("Umrel na Vanoce");
        newBook.setCharacters(new ArrayList<>(Arrays.asList(stromak, smrk)));
        newBook.setComposition("Kompozice spadaneho listi");
        newBook.setExtraInfo("Extra info o lese");

        Book createdBook = bookService.saveBook(newBook);

        assertThat(createdBook).isNotNull();
    }

    @Test
    @Transactional
    void createBookOnlyWithMandatoryInfoFilled(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);

        Book createdBook = bookService.saveBook(newBook);

        assertThat(createdBook).isNotNull();
    }

    @Test
    @Transactional
    void createBookWithoutNameShouldFail(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setLitForm("Drama");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);

        try {
            bookService.saveBook(newBook);
            Assertions.fail();
        }catch (DataIntegrityViolationException ignored){}

    }

    @Test
    @Transactional
    void createBookWithoutFormShouldFail(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setBookName("O kremilkovi");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);

        try {
            bookService.saveBook(newBook);
            Assertions.fail();
        }catch (DataIntegrityViolationException | NullPointerException ignored){}

    }

    @Test
    @Transactional
    void createBookWithoutOriginShouldFail(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setLitForm("Drama");
        newBook.setBookName("O kremilkovi");
        newBook.setFirstEdition(1957);

        try {
            bookService.saveBook(newBook);
            Assertions.fail();
        }catch (DataIntegrityViolationException | NullPointerException ignored){}

    }

    @Test
    @Transactional
    void getExistingBook(){
        CompleteBookForm newBook = new CompleteBookForm();

        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setLitGenres(new ArrayList<>(Arrays.asList("Fantasy", "Novela")));
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);
        newBook.setAuthorName("Karel Kastan");
        newBook.setAuthorInfo("Spadl z kastanu");
        newBook.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook.setHistoryContext("Velky spatny");
        newBook.setLiteraryContext("Velky dobry");
        newBook.setContemporaryAuthors(new ArrayList<>(Arrays.asList("Pepa", "Vasek")));
        newBook.setTheme("O osudu stromu");
        newBook.setMotives("Padani listi");
        newBook.setTime("Davno");
        newBook.setSpace("V lese");
        newBook.setPlot("O umirani stromu");
        CharacterTransport stromak = new CharacterTransport();
        stromak.setName("Stromak");
        stromak.setDescription("Stromopan");
        CharacterTransport smrk = new CharacterTransport();
        stromak.setName("Smrcek");
        stromak.setDescription("Umrel na Vanoce");
        newBook.setCharacters(new ArrayList<>(Arrays.asList(stromak, smrk)));
        newBook.setComposition("Kompozice spadaneho listi");
        newBook.setExtraInfo("Extra info o lese");

        Book createdBook = bookService.saveBook(newBook);

        CompleteBookForm foundBook = bookService.getBookById(createdBook.getId());

        assertThat(foundBook).isNotNull();
        assertThat(foundBook.getBookName()).isEqualTo(newBook.getBookName());
        assertThat(foundBook.getLitForm().toLowerCase()).isEqualTo(newBook.getLitForm().toLowerCase());
        assertThat(foundBook.getLitGenres()).containsAll(newBook.getLitGenres());
        assertThat(foundBook.getIsCzechOrWorld().toLowerCase()).isEqualTo(newBook.getIsCzechOrWorld().toLowerCase());
        assertThat(foundBook.getFirstEdition()).isEqualTo(newBook.getFirstEdition());
        assertThat(foundBook.getAuthorName()).isEqualTo(newBook.getAuthorName());
        assertThat(foundBook.getAuthorInfo()).isEqualTo(newBook.getAuthorInfo());
        assertThat(foundBook.getAuthorBooksOther()).containsAll(newBook.getAuthorBooksOther());
        assertThat(foundBook.getLiteraryContext()).isEqualTo(newBook.getLiteraryContext());
        assertThat(foundBook.getHistoryContext()).isEqualTo(newBook.getHistoryContext());
        assertThat(foundBook.getContemporaryAuthors()).containsAll(newBook.getContemporaryAuthors());
        assertThat(foundBook.getTheme()).isEqualTo(newBook.getTheme());
        assertThat(foundBook.getMotives()).isEqualTo(newBook.getMotives());
        assertThat(foundBook.getTime()).isEqualTo(newBook.getTime());
        assertThat(foundBook.getSpace()).isEqualTo(newBook.getSpace());
        assertThat(foundBook.getPlot()).isEqualTo(newBook.getPlot());
        assertThat(foundBook.getCharacters()).containsAll(newBook.getCharacters());
        assertThat(foundBook.getComposition()).isEqualTo(newBook.getComposition());
        assertThat(foundBook.getExtraInfo()).isEqualTo(newBook.getExtraInfo());
    }

    @Transactional
    @Test
    void getBookWithInvalidIdShouldFail(){
        try {
            bookService.getBookById(99999L);
            Assertions.fail();
        } catch (IllegalArgumentException ignored){}
    }


    @Transactional
    @Test
    void updateBook(){
        CompleteBookForm newBook = new CompleteBookForm();

        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setLitGenres(new ArrayList<>(Arrays.asList("Fantasy", "Novela")));
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);
        newBook.setAuthorName("Karel Kastan");
        newBook.setAuthorInfo("Spadl z kastanu");
        newBook.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook.setHistoryContext("Velky spatny");
        newBook.setLiteraryContext("Velky dobry");
        newBook.setContemporaryAuthors(new ArrayList<>(Arrays.asList("Pepa", "Vasek")));
        newBook.setTheme("O osudu stromu");
        newBook.setMotives("Padani listi");
        newBook.setTime("Davno");
        newBook.setSpace("V lese");
        newBook.setPlot("O umirani stromu");
        CharacterTransport stromak = new CharacterTransport();
        stromak.setName("Stromak");
        stromak.setDescription("Stromopan");
        CharacterTransport smrk = new CharacterTransport();
        stromak.setName("Smrcek");
        stromak.setDescription("Umrel na Vanoce");
        newBook.setCharacters(new ArrayList<>(Arrays.asList(stromak, smrk)));
        newBook.setComposition("Kompozice spadaneho listi");
        newBook.setExtraInfo("Extra info o lese");

        Book createdBook = bookService.saveBook(newBook);

        CompleteBookForm updatedBook = new CompleteBookForm();

        updatedBook.setBookName("O betonu");
        updatedBook.setLitForm("Poezie");
        updatedBook.setLitGenres(new ArrayList<>(Arrays.asList("Lyrika", "Balada")));
        updatedBook.setIsCzechOrWorld("czech");
        updatedBook.setFirstEdition(2020);
        updatedBook.setAuthorName("Karel Sloup");
        updatedBook.setAuthorInfo("Spadl ze sloupu");
        updatedBook.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O travice", "O lasce")));
        updatedBook.setHistoryContext("Velky moderni");
        updatedBook.setLiteraryContext("Velky stary");
        updatedBook.setContemporaryAuthors(new ArrayList<>(Arrays.asList("Lubos", "Milan")));
        updatedBook.setTheme("O osudu mesta");
        updatedBook.setMotives("Detonace parku");
        updatedBook.setTime("Dnes");
        updatedBook.setSpace("Ve meste");
        updatedBook.setPlot("O rozkvetu mesta");
        CharacterTransport mestak = new CharacterTransport();
        stromak.setName("Panelak");
        stromak.setDescription("Peti patrovy");
        CharacterTransport vesnican = new CharacterTransport();
        stromak.setName("Statek");
        stromak.setDescription("S konma i prasaty");
        updatedBook.setCharacters(new ArrayList<>(Arrays.asList(mestak, vesnican)));
        updatedBook.setComposition("Kompozice sedych budov");
        updatedBook.setExtraInfo("Extra info o meste");

        Book newBookInfo = bookService.updateBook(createdBook.getId(), updatedBook);


        assertThat(newBookInfo).isNotNull();
        assertThat(newBookInfo.getName()).isEqualTo(updatedBook.getBookName());
        assertThat(newBookInfo.getLiteraryForm().toString().toLowerCase()).isEqualTo("poetry");
        assertThat(newBookInfo.getGenres()).containsAll(updatedBook.getLitGenres());
        assertThat(newBookInfo.getOrigin().toString().toLowerCase()).isEqualTo(updatedBook.getIsCzechOrWorld().toLowerCase());
        assertThat(newBookInfo.getFirstEdition()).isEqualTo(updatedBook.getFirstEdition());
        assertThat(newBookInfo.getAuthor()).isNotNull();
        assertThat(newBookInfo.getAuthor().getName()).isEqualTo(updatedBook.getAuthorName());
        assertThat(newBookInfo.getAuthor().getInfo()).isEqualTo(updatedBook.getAuthorInfo());
        assertThat(newBookInfo.getAuthor().getOtherBooks()).containsAll(updatedBook.getAuthorBooksOther());
        assertThat(newBookInfo.getLiteraryContext()).isEqualTo(updatedBook.getLiteraryContext());
        assertThat(newBookInfo.getHistoryContext()).isEqualTo(updatedBook.getHistoryContext());
        assertThat(newBookInfo.getContemporaryAuthors()).containsAll(updatedBook.getContemporaryAuthors());
        assertThat(newBookInfo.getBookContent()).isNotNull();
        assertThat(newBookInfo.getBookContent().getTheme()).isEqualTo(updatedBook.getTheme());
        assertThat(newBookInfo.getBookContent().getMotives()).isEqualTo(updatedBook.getMotives());
        assertThat(newBookInfo.getBookContent().getTime()).isEqualTo(updatedBook.getTime());
        assertThat(newBookInfo.getBookContent().getSpace()).isEqualTo(updatedBook.getSpace());
        assertThat(newBookInfo.getBookContent().getPlot()).isEqualTo(updatedBook.getPlot());
        for (int i = 0; i < newBookInfo.getBookContent().getCharacters().size(); ++i){
            assertThat(newBookInfo.getBookContent().getCharacters().get(i).getName()).isEqualTo(updatedBook.getCharacters().get(i).getName());
            assertThat(newBookInfo.getBookContent().getCharacters().get(i).getDescription()).isEqualTo(updatedBook.getCharacters().get(i).getDescription());
        }
        assertThat(newBookInfo.getBookContent().getComposition()).isEqualTo(updatedBook.getComposition());
        assertThat(newBookInfo.getBookContent().getExtraInfo()).isEqualTo(updatedBook.getExtraInfo());
    }

    @Transactional
    @Test
    void removingMandatoryInfoByUpdateBookShouldFail(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);

        Book createdBook = bookService.saveBook(newBook);

        CompleteBookForm updateRequest = new CompleteBookForm();
        updateRequest.setLitForm("Poezie");
        updateRequest.setIsCzechOrWorld("czech");
        updateRequest.setFirstEdition(1960);

        try {
            bookService.updateBook(createdBook.getId(), updateRequest);
            Assertions.fail();
        } catch (DataIntegrityViolationException ignored){}
    }

    @Transactional
    @Test
    void deletingBook(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);

        Book createdBook = bookService.saveBook(newBook);

        bookService.deleteBookById(createdBook.getId());

        try {
            bookService.getBookById(createdBook.getId());
            Assertions.fail();
        } catch (IllegalArgumentException ignored){}
    }

    @Transactional
    @Test
    void copyContextFrom20century(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1957);
        newBook.setAuthorName("Karel");
        newBook.setAuthorInfo("Karel o karlovi");
        newBook.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook.setHistoryContext("Kontext Karla");

        CompleteBookForm newBook2 = new CompleteBookForm();
        newBook2.setBookName("O kremilkovi");
        newBook2.setLitForm("Drama");
        newBook2.setIsCzechOrWorld("czech");
        newBook2.setFirstEdition(1980);
        newBook2.setAuthorName("Jachym");
        newBook2.setAuthorInfo("Jachym o karlovi");
        newBook2.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook2.setHistoryContext("Kontext Jachyma");

        CompleteBookForm newBook3 = new CompleteBookForm();
        newBook3.setBookName("O kremilkovi");
        newBook3.setLitForm("Drama");
        newBook3.setIsCzechOrWorld("world");
        newBook3.setFirstEdition(1950);
        newBook3.setAuthorName("Rudolf");
        newBook3.setAuthorInfo("Rudolf o karlovi");
        newBook3.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook3.setHistoryContext("Kontext Rudolfa");

        CompleteBookForm newBook4 = new CompleteBookForm();
        newBook4.setBookName("O kremilkovi");
        newBook4.setLitForm("Drama");
        newBook4.setIsCzechOrWorld("world");
        newBook4.setFirstEdition(1915);
        newBook4.setAuthorName("Starec");
        newBook4.setAuthorInfo("Starec o karlovi");
        newBook4.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook4.setHistoryContext("Kontext Starce");

        bookService.saveBook(newBook);
        bookService.saveBook(newBook2);
        bookService.saveBook(newBook3);
        bookService.saveBook(newBook4);


        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setAuthorName(newBook.getAuthorName());
        contextInfo.setFirstEdition(1950);
        contextInfo.setCzech(false);
        List<ContextForm> contexts = bookService.getContexts("history", contextInfo);
        assertThat(contexts.size()).isGreaterThan(2);
        assertThat(contexts.get(0).getContext()).isEqualTo("Kontext Karla");
        assertThat(contexts).doesNotContain(new ContextForm(newBook4.getBookName(), newBook4.getHistoryContext()));
    }

    @Transactional
    @Test
    void copyContextFrom19century(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(1800);
        newBook.setAuthorName("Karel");
        newBook.setAuthorInfo("Karel o karlovi");
        newBook.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook.setHistoryContext("Kontext Karla");

        CompleteBookForm newBook2 = new CompleteBookForm();
        newBook2.setBookName("O kremilkovi");
        newBook2.setLitForm("Drama");
        newBook2.setIsCzechOrWorld("czech");
        newBook2.setFirstEdition(1900);
        newBook2.setAuthorName("Jachym");
        newBook2.setAuthorInfo("Jachym o karlovi");
        newBook2.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook2.setHistoryContext("Kontext Jachyma");

        CompleteBookForm newBook3 = new CompleteBookForm();
        newBook3.setBookName("O kremilkovi");
        newBook3.setLitForm("Drama");
        newBook3.setIsCzechOrWorld("world");
        newBook3.setFirstEdition(1850);
        newBook3.setAuthorName("Rudolf");
        newBook3.setAuthorInfo("Rudolf o karlovi");
        newBook3.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook3.setHistoryContext("Kontext Rudolfa");

        CompleteBookForm newBook4 = new CompleteBookForm();
        newBook4.setBookName("O kremilkovi");
        newBook4.setLitForm("Drama");
        newBook4.setIsCzechOrWorld("world");
        newBook4.setFirstEdition(1915);
        newBook4.setAuthorName("Starec");
        newBook4.setAuthorInfo("Starec o karlovi");
        newBook4.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook4.setHistoryContext("Kontext Starce");

        bookService.saveBook(newBook);
        bookService.saveBook(newBook2);
        bookService.saveBook(newBook3);
        bookService.saveBook(newBook4);


        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setAuthorName("Jarmil");
        contextInfo.setFirstEdition(1850);
        contextInfo.setCzech(false);
        List<ContextForm> contexts = bookService.getContexts("history", contextInfo);
        assertThat(contexts.size()).isGreaterThan(2);
        assertThat(contexts).doesNotContain(new ContextForm(newBook4.getBookName(), newBook4.getHistoryContext()));
    }

    @Transactional
    @Test
    void copyContextFromBefore18century(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(503);
        newBook.setAuthorName("Karel");
        newBook.setAuthorInfo("Karel o karlovi");
        newBook.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook.setHistoryContext("Kontext Karla");

        CompleteBookForm newBook2 = new CompleteBookForm();
        newBook2.setBookName("O kremilkovi");
        newBook2.setLitForm("Drama");
        newBook2.setIsCzechOrWorld("czech");
        newBook2.setFirstEdition(612);
        newBook2.setAuthorName("Jachym");
        newBook2.setAuthorInfo("Jachym o karlovi");
        newBook2.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook2.setHistoryContext("Kontext Jachyma");

        CompleteBookForm newBook3 = new CompleteBookForm();
        newBook3.setBookName("O kremilkovi");
        newBook3.setLitForm("Drama");
        newBook3.setIsCzechOrWorld("world");
        newBook3.setFirstEdition(402);
        newBook3.setAuthorName("Rudolf");
        newBook3.setAuthorInfo("Rudolf o karlovi");
        newBook3.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook3.setHistoryContext("Kontext Rudolfa");

        CompleteBookForm newBook4 = new CompleteBookForm();
        newBook4.setBookName("O kremilkovi");
        newBook4.setLitForm("Drama");
        newBook4.setIsCzechOrWorld("world");
        newBook4.setFirstEdition(1915);
        newBook4.setAuthorName("Starec");
        newBook4.setAuthorInfo("Starec o karlovi");
        newBook4.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));
        newBook4.setHistoryContext("Kontext Starce");

        bookService.saveBook(newBook);
        bookService.saveBook(newBook2);
        bookService.saveBook(newBook3);
        bookService.saveBook(newBook4);


        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setAuthorName(newBook.getAuthorName());
        contextInfo.setFirstEdition(500);
        contextInfo.setCzech(true);
        List<ContextForm> contexts = bookService.getContexts("history", contextInfo);
        assertThat(contexts.size()).isGreaterThan(1);
        assertThat(contexts).doesNotContain(new ContextForm(newBook4.getBookName(), newBook4.getHistoryContext()));
        assertThat(contexts).doesNotContain(new ContextForm(newBook2.getBookName(), newBook2.getHistoryContext()));
    }

    @Transactional
    @Test
    void copyContemporaryAuthors(){
        CompleteBookForm newBook = new CompleteBookForm();
        newBook.setBookName("O kremilkovi");
        newBook.setLitForm("Drama");
        newBook.setIsCzechOrWorld("world");
        newBook.setFirstEdition(-512);
        newBook.setAuthorName("Karel");
        newBook.setAuthorInfo("Karel o karlovi");
        newBook.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));

        CompleteBookForm newBook2 = new CompleteBookForm();
        newBook2.setBookName("O kremilkovi");
        newBook2.setLitForm("Drama");
        newBook2.setIsCzechOrWorld("czech");
        newBook2.setFirstEdition(-621);
        newBook2.setAuthorName("Jachym");
        newBook2.setAuthorInfo("Jachym o karlovi");
        newBook2.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));

        CompleteBookForm newBook3 = new CompleteBookForm();
        newBook3.setBookName("O kremilkovi");
        newBook3.setLitForm("Drama");
        newBook3.setIsCzechOrWorld("world");
        newBook3.setFirstEdition(-530);
        newBook3.setAuthorName("Rudolf");
        newBook3.setAuthorInfo("Rudolf o karlovi");
        newBook3.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));

        CompleteBookForm newBook4 = new CompleteBookForm();
        newBook4.setBookName("O kremilkovi");
        newBook4.setLitForm("Drama");
        newBook4.setIsCzechOrWorld("world");
        newBook4.setFirstEdition(1915);
        newBook4.setAuthorName("Starec");
        newBook4.setAuthorInfo("Starec o karlovi");
        newBook4.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));

        CompleteBookForm newBook5 = new CompleteBookForm();
        newBook5.setBookName("O kremilkovi");
        newBook5.setLitForm("Drama");
        newBook5.setIsCzechOrWorld("world");
        newBook5.setFirstEdition(-560);
        newBook5.setAuthorName("Ludmil");
        newBook5.setAuthorInfo("Ludmil o karlovi");
        newBook5.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));

        CompleteBookForm newBook6 = new CompleteBookForm();
        newBook6.setBookName("O kremilkovi");
        newBook6.setLitForm("Drama");
        newBook6.setIsCzechOrWorld("czech");
        newBook6.setFirstEdition(-590);
        newBook6.setAuthorName("Jiri");
        newBook6.setAuthorInfo("Jiri o karlovi");
        newBook6.setAuthorBooksOther(new ArrayList<>(Arrays.asList("O perniku", "O snehu")));


        bookService.saveBook(newBook);
        bookService.saveBook(newBook2);
        bookService.saveBook(newBook3);
        bookService.saveBook(newBook4);
        bookService.saveBook(newBook5);
        bookService.saveBook(newBook6);

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setAuthorName("Nekdo");
        contextInfo.setFirstEdition(-550);
        contextInfo.setCzech(true);
        List<String> contemporaryAuthors = bookService.getContemporaryAuthors(contextInfo);
        assertThat(contemporaryAuthors.size()).isEqualTo(4);
        assertThat(contemporaryAuthors.get(0)).isEqualTo(newBook5.getAuthorName());
        assertThat(contemporaryAuthors.get(1)).isEqualTo(newBook3.getAuthorName());
        assertThat(contemporaryAuthors.get(2)).isEqualTo(newBook6.getAuthorName());
        assertThat(contemporaryAuthors.get(3)).isEqualTo(newBook.getAuthorName());
    }

}
